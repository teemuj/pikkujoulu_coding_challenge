# Pikkujoulu coding challenge 
## Ongelma
Miten jakaa N henkil�� M ryhm��n. 
### Ohjelman kuvaus
Ohjelma lukee sy�tteen STDIN:st� ja tulostaa ratkaisun STDOUT:n.

Sy�tteen ensimm�isell� rivill� annetaan ryhmien nimet pilkulla erotettuna.
T�m�n j�lkeen luetaan rivej� kunnes sy�te on luettu loppuun. Rivit ovat seuraavaa muotoa
~~~
nimi[,ryhm�n j�rjestysnumero (1-based) johon henkil� ei voi kuulua]*
~~~
Rivit voi olettaa loppuvan `\n` (`0x0A`) -merkkiin

Alla esimerkkisy�te ja yksi mahdollinen ratkaisu.
### Esimerkkisy�te
~~~
A,B,C
Roope,2,3
Aku,1,2
Mikki
Hessu,3
~~~
### Esimerkkituloste
~~~
A: Mikki, Roope
B: Hessu
C: Aku
~~~
Huomioita tulosteesta: Ryhm�t samassa j�rjestyksess� kuin sy�tteen�. Ryhm��n kuuluvien nimet aakkosj�rjestyksess� (tai jossain muussa j�rjestyksess� jos aakkostaminen on vaikeaa) pilkulla erotettuina. 
## Ratkaisu
Ratkaisun voi toteuttaa yo. ohjeita noudattaen haluamallaan ohjelmointikielell�. 
Ratkaisut palautetaan pull requesteina t�h�n projektiin niin ett� `solutions` kansion alle lis�t��n oman nimen mukainen kansio 
joka sis�lt�� ratkaisun l�hdekoodin. Ohjelman suoritus on pystytt�v� demoamaan (kts. bonus 1).

Jos koodaus kuullostaa pelottavalta voi ratkaisun tehd� esim. Power Pointilla, Excelill� tai muulla v�hemm�n pelottavalla ty�kalulla. Lis�ksi palautuksen voi suorittaa esim. s�hk�postilla.

Oleellista on pysty� demoamaan oma toteutus annetulla sy�tteell�.

## Pohjat ratkaisuille
`templates` kansio sis�lt�� valmiita pohjia joita voi halutessaa k�ytt�� ratkaisujen toteuttamiseen. Kansioon voi halutessaan lis�t� pohjan (Pull requestina) uudelle ymp�rist�lle helpottamaan muiden tekemisi�. 

## Bonus 1
Lis�� ratkaisuun Dockerfile ja tarvittavat tiedostot joista pystyy rakentamaan containerin, jolla ohjelman pystyy ajamaan.

## Palkinnot
Mainetta, kunniaa ja ihailua. Mahdollisesti my�s jotain konkreettista.

## Q&A

### Q1: Miten toimitaan jos k�ytt�j�� ei voi laittaa mihink��n ryhm��n?
A: Ei laiteta k�ytt�j�� mihink��n ryhm��n.

### Q2: Miten k�ytt�j�t tulisi jakaa ryhmiin ("voiko laittaa kaikki k�ytt�j�t samaan ryhm��n")
A: Sy�tteen asettamat rajoitteet huomioiden mahdollisimman tasaisesti.

## Deadline
23.11. klo 12:00

