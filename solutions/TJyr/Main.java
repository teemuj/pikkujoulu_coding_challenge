/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author teemu.jyrkama
 */
public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No file name given");
            return;
        }

        BufferedReader bReader = null;

        try {
            bReader = new BufferedReader(new FileReader(args[0]));
        } catch (IOException ioe) {
            System.out.println("Can't read the file");
            return;
        }

        List<Participant> participants = new LinkedList();
        List<Group> groups = null;
        
        GroupManager groupManager = new GroupManager();

        String theLine = null;

        try {
            String groupLine = bReader.readLine();
            if (groupLine == null) {
                System.out.println("The input file is malformed");
                return;
            }

            groups = parseAndAddGroupLine(groupLine,groupManager);

            while ((theLine = bReader.readLine()) != null) {
                Participant participant = parseParticipantLine(theLine);
                participants.add(participant);
                groupManager.addParticipantToGroup(participant);
            }
        } catch (Exception e) {
            System.out.println("The input file in malformed");
            return;
        }

        printGroups(groups);
    }

    public static List<Group> parseAndAddGroupLine(String line, GroupManager groupManager) throws Exception {
        String[] tokens = line.split(",");

        if (tokens.length == 0) {
            throw new Exception();
        }

        LinkedList<Group> groups = new LinkedList();

        for (int i = 0; i < tokens.length; i++) {
            Group group = new Group(tokens[i],i+1);
            groups.add(group);
            groupManager.addNewGroup(group);
        }

        return groups;
    }

    public static Participant parseParticipantLine(String line) throws Exception {
        String[] tokens = line.split(",");

        if (tokens.length == 0) {
            throw new Exception();
        }

        Participant participant = new Participant(tokens[0]);

        for (int i = 1; i < tokens.length; i++) {
            participant.addConstraint(Integer.parseInt(tokens[i]));
        }

        return participant;
    }

    public static void printGroups(List<Group> groups) {

        for (Group g : groups) {

            System.out.print(g.getId() + ":");

            int i = 0;

            if (g.getMemberCount() == 0)
                System.out.println("");
            
            for (Participant p : g.getMembers()) {

                if (i < g.getMemberCount() - 1) {
                    System.out.print(p.getName());
                    System.out.print(",");
                } else {
                    System.out.println(p.getName());
                }
            }
        }
    }
}
