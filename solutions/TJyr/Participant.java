

import java.util.List;
import java.util.LinkedList;

public class Participant {

    private String name;
    private List<Integer> constraints;
    private int finalGroup;

    public Participant(String name) {
        this.name = name;
        this.constraints = new LinkedList();
    }

    public void addConstraint(Integer constraint) {
        this.constraints.add(constraint);
    }

    public List<Integer> getConstraints() {
        return this.constraints;
    }

    public String getName() {
        return this.name;
    }
}
