

import java.util.List;
import java.util.LinkedList;

public class Group {

    private String id;
    private List<Participant> members;
    private int memberCount;
    private int number;

    public Group(String id, int number) {
        this.id = id;
        this.members = new LinkedList();
        this.memberCount = 0;
        this.number = number;
    }

    public String getId() {
        return this.id;
    }

    public int getNumber() {
        return this.number;
    }

    public void addMember(Participant participant) {
        this.members.add(participant);
        this.memberCount++;
    }

    public int getMemberCount() {
        return this.memberCount;
    }

    public List<Participant> getMembers() {
        return this.members;
    }
}
