/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author teemu.jyrkama
 */
public class GroupManager {
    
    List<List<Group>> groups;
    
    public GroupManager() {
        List<Group> firstGroup = new LinkedList();
        this.groups = new LinkedList();
        this.groups.add(firstGroup);
    }
    
    public void addNewGroup(Group group) {
        this.groups.get(0).add(group);
    }
    
    public void addParticipantToGroup(Participant participant) {
        
        Iterator<List<Group>> groupListIterator = this.groups.iterator();
        
        // iterating over groups list list
        while (groupListIterator.hasNext()) {
            
            boolean found = false;
            
            List<Group> groupList = groupListIterator.next();
            Iterator<Group> groupIterator = groupList.iterator();
            
            // iterating over list of groups
            while (groupIterator.hasNext()) {
                
                Group group = groupIterator.next();
                Iterator<Integer> contraintIterator = participant.getConstraints().iterator();
                boolean add = true;
                
                //System.out.print("Constraints");
                // iterate over constraints
                while (contraintIterator.hasNext()) {
                    Integer constraint = contraintIterator.next();
                    //System.out.print(constraint);
                    
                    if (constraint == group.getNumber()) {
                        add = false;
                        break;
                    }
                }
                
                // add if there there are no constraint
                if (add) {
                    group.addMember(participant);
                    found = true;
                    
                    groupIterator.remove();
                    
                    if (groupListIterator.hasNext()) {
                        List<Group> listToAdd = groupListIterator.next();
                        listToAdd.add(group);
                    }
                    else {
                        LinkedList<Group> newList = new LinkedList();
                        newList.add(group);
                        this.groups.add(newList);
                    }
                    break;
                }
            }
            
            if (found)
                break;
        }
    }
    
}
